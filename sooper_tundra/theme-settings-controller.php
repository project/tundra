<?php

// Load the custom theme options CSS file
GLOBAL $theme;
$cto_file = $files_path.'/cto-'.$theme.'.css';
if (file_exists($cto_file)) {
  drupal_add_css($data = $cto_file, array('type' => 'file','group' => CSS_THEME, 'weight' => 999));
}


// Load jQuery equal heights plugin
if (theme_get_setting('force_eq_heights')) {
  if(is_file($theme_path .'scripts/misc/jQuery.equalHeights.js')) {
    drupal_add_js($theme_path .'scripts/misc/jQuery.equalHeights.js');
  }
}

// Load SOOPER Features
foreach (file_scan_directory($theme_path .'features', '/controller.inc/i') as $file) {
  include($file->uri);
}