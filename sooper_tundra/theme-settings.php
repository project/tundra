<?php

/*
zoek nieuwe manier om tundra te enablen op theme settings pagina

krumo($GLOBALS);
global $conf;
$conf['admin_theme'] = 'sooper_tundra';
global $theme;
$theme = 'sooper_tundra';
global $theme_path;
$theme_path = 'sites/all/themes/sooper_tundra';
krumo($GLOBALS);

*/

/**
 * Implements hook_form_system_theme_settings_alter().
 *
 * @param $form
 *   The form.
 * @param $form_state
 *   The form state.
 */

 
 
function sooper_tundra_form_system_theme_settings_alter(&$form, $form_state) {
$form['theme_settings']['#weight'] = 20;
$form['logo']['#weight'] = 21;
$form['favicon']['#weight'] = 22;

  $form['#submit'][] = 'sooper_configurator_submit';

  /* Store theme paths in variables - if you rename the theme you have to change the second parameter of the drupal_get_path function on the next line */
  $theme_path = drupal_get_path('theme', arg(3)) .'/';
  $abs_theme_path = base_path().$theme_path;

  // Variable that contains easing options. Chose not to use function because $theme_path would be unavailable
  if (file_exists($theme_path . 'scripts/misc/jquery.easing-sooper.js')) {
    $easing_options = array (
    'linear' => 'linear',
    'swing' => 'swing',
    'easeInTurbo' => 'easeInTurbo',
    'easeOutTurbo' => 'easeOutTurbo',
    'easeInTurbo2' => 'easeInTurbo2',
    'easeOutTurbo2' => 'easeOutTurbo2',
    'easeInTurbo3' => 'easeInTurbo3',
    'easeOutTurbo3' => 'easeOutTurbo3',
    'easeInSine' => 'easeInSine',
    'easeOutSine' => 'easeOutSine',
    'easeInExpo' => 'easeInExpo',
    'easeOutExpo' => 'easeOutExpo',
    'easeInCirc' => 'easeInCirc',
    'easeOutCirc' => 'easeOutCirc',
    'easeInElastic' => 'easeInElastic',
    'easeOutElastic' => 'easeOutElastic',
    'easeInOvershoot' => 'easeInOvershoot',
    'easeOutOvershoot' => 'easeOutOvershoot',
    'easeInOvershootTurbo' => 'easeInOvershootTurbo',
    'easeOutOvershootTurbo' => 'easeOutOvershootTurbo',
    'easeInBounce' => 'easeInBounce',
    'easeOutBounce' => 'easeOutBounce',
    );
  } else {
    $easing_options = array (
    'linear' => 'linear',
    'swing' => 'swing',
    );
  }



  // Create the form widgets using Forms API

  // Load SOOPER Features
  foreach (file_scan_directory($theme_path .'features', '/settings.inc/i') as $file) {
    include($file->uri);
  }

  $form['misc'] = array(
    '#title' => t('Other Options'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 10,
	);

  $form['misc']['block_edit'] = array(
    '#type' => 'radios',
    '#title' => t('Edit-Block links'),
    '#default_value' => theme_get_setting('block_edit'),
    '#options' => array(
     '0' => t('No'),
     '1' => t('Yes'),
    ),
    '#description' => t('Add edit links on all blocks to allow easy access to blocks management.'),
  );

	$form['misc']['force_eq_heights'] = array(
    '#type' => 'radios',
    '#title' => t('Enforce Equal heights'),
    '#default_value' => theme_get_setting('force_eq_heights'),
    '#options' => array(
     '0' => t('No'),
     '1' => t('Yes'),
    ),
    '#description' => t('Enforce equal heights on horizontally aligned blocks (it looks better).'),
  );

  $form['misc']['iepngfix'] = array(
    '#type' => 'radios',
    '#title' => t('Use IE PNG Fix'),
    '#default_value' => theme_get_setting('iepngfix'),
    '#options' => array(
     '0' => t('No'),
     '1' => t('Yes'),
    ),
    '#description' => t('Fix Alpha-transparency in PNG support for internet explorer 6.'),
  );

  $form['dev'] = array(
    '#title' => t('Developer Options'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 11,
	);


  /**
   * Validation Function to enforce numeric values
   */
  function is_number($formelement, &$form_state) {
    $thevalue = $formelement['#value'];
    $title = $formelement['#title'];
    if (!is_numeric($thevalue)) {
      form_error($formelement, t("<em>$title</em> must be a number."));
    }
  }
  // Return the additional form widgets
  return $form;
}

/**
 * Helper function to provide a list of sizes for use in theme settings.
 * Originally by Jacine @ Sky theme - thx ;)
 */
function sooper_size_range($start = 11, $end = 16, $unit = false, $default = NULL, $granularity = 1) {
  $range = '';
  if (is_numeric($start) && is_numeric($end)) {
    $range = array();
    $size = $start;
    while ($size >= $start && $size <= $end) {
      if ($size == $default) {
        $range[$size . $unit] = $size . $unit .' (default)';
      }
      else {
        $range[$size . $unit] = $size . $unit;
      }
      $size += $granularity;
    }
  }
  return $range;
}

function sooper_configurator_submit($form, &$form_state) {
  
  $files_path = file_stream_wrapper_get_instance_by_scheme('public')->getDirectoryPath();
  $theme = arg(3);
  $theme_path = drupal_get_path('theme', arg(3)) .'/';
  $abs_theme_path = base_path().$theme_path;
  $ctoCssFile = $files_path.'/cto-'.$theme.'.css';
  /* Construct CSS file: */
  $CSS = '';

  // Load SOOPER Features
  foreach (file_scan_directory($theme_path .'features', '/css.inc/i') as $file) {
    include($file->uri);
  }

  /**
  * Color module injection
  * Writes the colors to the stylesheet, using the color_get_palette('themename') function, and assisting color-calibration functions if applicable
  * (color module's own color shifting in CSS works unreliably and inaccurately)
  */
  if (module_exists('color')){
    if(is_file($theme_path . 'color/color-utility-functions.inc')) {
    include('color/color-utility-functions.inc');
    }
  // Override colors in style.css with color palette from theme settings. All selectors have increased specificity so that they will override other stylesheets and not be overriden
    $ctopalette = $form_state['values']['palette'];
    $CSS .= "html body, body h1, body h2, body h3, body h4, body h5, body h6, body h1 a, body h2 a, body h3 a, body h4 a, body h5 a, body h6 a, body .item-list li a, body #toolbar input.form-text,body #content .node .node-data h1.nodetitle a { color:".$ctopalette['text'].";}\n";
    $CSS .= "body .poll .bar, body #content ul.secondary li a, body #container ul.primary li a:hover, body #container ul.primary li a:focus, body ul.primary li a, body div.block div.edit a, body #toolbar, body body .skinr-color-3 .block-inner { background-color:".$ctopalette['text'].";}\n";
    $CSS .= "body a, body #content ul.secondary li a,body h1#sitename a, body #main #content .nodetitle a:hover,body #main #content .nodetitle a:focus, body .item-list li, body span.form-required, body th a:link, body th a:visited,body ul.quicktabs_tabs.quicktabs-style-tundra li a { color:".$ctopalette['base'].";}\n";
    $CSS .= "body #content ul.pager li,body ul.secondary-links li a, body #cycle-pager a,body #content ul.secondary li a.active, body input.form-submit,body #navbar, body #navbar ul ul, body ul.primary li.active a,body .poll .bar .foreground,body .skinr-color,body ul.quicktabs_tabs.quicktabs-style-tundra li.active a { background-color:".$ctopalette['base'].";}\n";
    $CSS .= "body .slideshow,body #cycle-pager,body input.form-text:focus, body input.form-upload:focus, body input.form-file:focus, body textarea:focus { border-color:".$ctopalette['base'].";}\n";
    $CSS .= "body .comment .comment-meta, body .comment .comment-meta a, body .node-full .node-meta p.date, body .node-teaser .node-meta div.links a, body blockquote,body #content .node-teaser  .node-meta { color:".$ctopalette['link'].";}\n";
    $CSS .= "body #footer,body .comment .comment-meta, body blockquote,body #content .node-teaser  .node-meta { border-color:".$ctopalette['link'].";}\n";
    $CSS .= "body .skinr-color-2 .block-inner { background-color:".$ctopalette['link'].";}\n";
  }

  $fh = fopen($ctoCssFile, 'w');
  if ($fh) {
    fwrite($fh, $CSS); // write css to file
  } else {
    drupal_set_message(t('Cannot write theme-settings file, please check your file system. (Visit status report page)'), 'error');
  }
  fclose($fh);
  // If the CSS & JS aggregation performance options are enabled we need to clear the caches
  drupal_clear_css_cache();
  drupal_clear_js_cache();

}