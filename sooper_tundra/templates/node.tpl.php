<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix grid-unit"<?php print $attributes; ?>>
  <div class="node-meta">
  
  <?php print $user_picture; ?>
  
  <?php if ($display_submitted): ?>
      <?php
        print '<p class="author">'. t('by ') . $name .'</p> <p class="date">'. format_date($created, 'custom', "F jS, Y") .'</p>';
      ?>
  <?php endif; ?>

  <?php print render($content['links']); ?>

  <?php if (!empty($content['links']['terms'])): ?>
    <div class="terms">
      <?php print render($content['links']['terms']); ?>
    </div>
  <?php endif; ?>
  </div>
  
  <div class="node-data">

    <?php print render($title_prefix); ?>
    <?php if (!$page): ?>
      <h1<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h1>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    
    <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php 
      hide($content['links']);
      hide($content['field_tags']);
      hide($content['comments']);
      print render($content); 
    ?>
    </div>
  
  </div>
</div><!-- end .node -->
<?php print render($content['comments']); ?>