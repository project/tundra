<div id="container" class="clearifx sooper-mast">

  <div id="header"><div class="inner clearfix">
    <?php if ($logo): ?>
    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
      <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
    </a>
    <?php endif; ?>

    <?php if ($site_name): ?>
    <h1 id="sitename"><a href="<?php print url() ?>" title="<?php t('Home') ?>"><?php print($site_name) ?></a></h1>
    <?php endif;?>

    <?php if ($site_slogan): ?>
    <p class="slogan"><?php print $site_slogan ?></p>
    <?php endif;?>

    <?php if ($secondary_menu): ?>
      <?php print theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary-menu', 'class' => array('links', 'clearfix')), 'heading' => t('Secondary menu'))); ?>
    <?php endif; ?>
  </div></div><!-- end .inner --><!-- end #header -->

  <div id="main">

  <?php if ($main_menu): ?>
  <div id="navbar" class="lone-grid-unit">
      <?php print $main_menu; ?>
  </div><!-- end #navbar -->
  <?php endif; ?>

  <?php if ($page['preblocks']): ?>
  <div class="sooperblocks clearfix preblocks">
  <?php print render($page['preblocks']); ?>
  </div><!-- end .preblocks -->
  <?php endif; ?>

  <div class="wrapcolumns">
  <?php if ($page['sidebar_first']): ?>
  <div class="sidebar side-first"><div class="side-inner">
  <?php print render($page['sidebar_first']); ?>
  </div></div><!-- end .inner --><!-- end sidebar first -->
  <?php endif; ?>

  <div id="content"><div class="content-inner grid-unit">

    <?php if ($page['slideshow']): ?>
      <div class="slideshow">
        <?php print render($page['slideshow']); ?>
        <div id="cycle-pager"></div>
      </div><!-- end .slideshow -->
    <?php endif; ?>
      
      
    <?php if ($breadcrumb) print $breadcrumb; ?>

    <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>

      <a name="content"></a>
      
      <?php print render($title_prefix); ?>
      <?php if ($title): ?><h1 class="title" id="content-title"><?php print $title; ?></h1><?php endif; ?>
      <?php print render($title_suffix); ?>      

      <?php if ($page['help']): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      
      <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>

      <?php if ($messages): ?>
        <div id="content-message"><?php print $messages ?></div>
      <?php endif; ?>

      <?php if ($page['content_top']): ?>
        <div class="content-top"><?php print render($page['content_top']); ?></div>
      <?php endif; ?>

      <?php print render($page['content']); ?>

      <?php if ($page['content_bottom']): ?>
        <div class="content-bottom"><?php print render($page['content_bottom']); ?></div>
      <?php endif; ?>

      <?php print $feed_icons; ?>

  </div></div><!-- end .inner --><!-- end #content -->

  <?php if ($page['sidebar_second']): ?>
  <div class="sidebar side-second"><div class="inner">
  <?php print render($page['sidebar_second']); ?>
  </div></div><!-- end .inner --><!-- end sidebar second -->
  <?php endif; ?>
  </div><!-- end .wrapcolumns -->

  <?php if ($page['postblocks']): ?>
  <div class="sooperblocks clearfix postblocks">
  <?php print render($page['postblocks']); ?>
  </div><!-- end .postblocks -->
  <?php endif; ?>

  </div><!-- end #main -->

  <div id="footer" class="clearfix">
    <?php if ($page['footer']): ?>
      <?php print render($page['footer']); ?>
    <?php endif; ?>
 	<?php if ($page['sooper_message']): // Since this is open source software you are allowed to remove this link to SooperThemes.com, but it's helpful to my website so if you could leave it, it's much appreciated! - Jurriaan (peach) ?>
	<?php print render($page['sooper_message']) ?>
    <?php endif; ?>
  </div><!-- end #footer -->

</div><!-- end #container -->

<?php if ($page['tundra-toolbar']): ?>
<div id="sooper-tundra-toolbar">
<?php print render($page['tundra-toolbar']); ?>
</div><!-- end .tundra-toolbar -->
<?php endif; ?>