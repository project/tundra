<div class="<?php print $comment_classes; ?>">
  <div class="comment-meta">

    <?php if ($status == 'unpublished'): ?>
      <p class="unpublished"><?php print t('Unpublished'); ?></p>
    <?php endif; ?>

    <?php print $picture; ?>

    <p class="submitted">
      <?php print $submitted; ?>
    </p>

    <?php if (isset($content['links'])): ?>
      <div class="links">
        <?php print render($content['links']); ?>
      </div>
    <?php endif; ?>
    
    <p class="comment-permalink">
      <?php print $permalink; ?>
    </p>
  </div><!-- end .meta -->

  <div class="comment-data">
    <?php if ($title): ?>
      <?php print render($title_prefix); ?>
      <h3 class="title comment-title"><?php print $title; if ($new): ?> <span class="new"><?php print $new; ?></span><?php endif; ?></h3>
      <?php print render($title_suffix); ?>
    <?php endif; ?>
    <div class="content"<?php print $content_attributes; ?>>
      <?php
        // We hide the links now so that we can render them elsewhere.
        hide($content['links']);
        print render($content);
      ?>
      <?php if ($signature): ?>
      <div class="signature clearfix">
        <?php print $signature; ?>
      </div>
      <?php endif; ?>
    </div> <!-- end .content -->
  </div>


</div><!-- end .comment -->