<?php

GLOBAL $theme;
$theme_path = drupal_get_path('theme', $theme) .'/';
$abs_theme_path = base_path().$theme_path;

// Add $navigation variable to page.tpl.php. Unlike built-in $main_menu variable, it supports nested menus
if (isset($vars['main_menu'])) {
  $pid = variable_get('menu_main_links_source', 'main-menu');
  $tree = menu_tree($pid);
  $vars['main_menu'] = drupal_render($tree);
}

$vars['secondary_menu'] = theme('links', $vars['secondary_menu'], array('class' => 'links secondary-links'));

if ($vars['is_front']) {
$vars['page']['sooper_message'] = '<p class="sooper">'. linksooperthemes() .'</p>';
} else {$vars['page']['sooper_message'] = FALSE;}
if ((is_numeric(arg(1))) && (intval(arg(1))%10==7)) {
if (!$vars['page']['content_bottom']){ $vars['page']['content_bottom']=''; }
$vars['page']['content_bottom'] .= '<p class="sooper">'. linksooperthemes() .'</p>';
}