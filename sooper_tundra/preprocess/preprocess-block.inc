<?php

$block = $vars['block'];

$vars['title_attributes_array']['class'][] = 'title';
$vars['title_attributes_array']['class'][] = 'blocktitle';

// Special classes for blocks
$vars['classes_array'][] = 'block-'. $block->module;
$vars['classes_array'][] = 'region-'. $vars['block_zebra'];
$vars['classes_array'][] = $vars['zebra'];
$vars['classes_array'][] = 'region-count-'. $vars['block_id'];
$vars['classes_array'][] = 'count-'. $vars['id'];
$vars['classes_array'][] = ($vars['block_id'] == 1) ? 'first' : '';
$vars['classes_array'][] = ($vars['block_id'] == count(block_list($block->region))) ? 'last' : '';