<?php

GLOBAL $theme;
$theme_path = drupal_get_path('theme', $theme) .'/';
$abs_theme_path = base_path().$theme_path;
$files_path = file_stream_wrapper_get_instance_by_scheme('public')->getDirectoryPath();

$vars['lte_ie6'] = '';
if(is_file($theme_path .'scripts/ie6/jQ.ie6hover.js')) {
  $vars['lte_ie6'] .= '<script type="text/javascript" src="'. $abs_theme_path .'scripts/ie6/jQ.ie6hover.js"></script>';
}
if (theme_get_setting('iepngfix') == 1) {
  if (is_file($theme_path .'scripts/ie6/iepngfix.htc')) {
  $vars['lte_ie6'] .= <<<EOD
  <style type="text/css">
  .content img {
  behavior: url({$abs_theme_path}scripts/ie6/iepngfix.htc);
  }
  </style>
EOD;
  }
}

// Widths of blocks in Dynamic regions
$precount=$postcount='';

if (isset($vars['page']['preblocks'])) {
$precount = count($vars['page']['preblocks'])-3;
}

if (isset($vars['page']['postblocks'])) {
$postcount = count($vars['page']['postblocks'])-3;

}
/**
* If you would like to give these blocks a sidemargin of 1% the width would be "$prewidth = round(100/$precount,3)-2;" The minus 2 is the sum of side-margin, side-border width and side-padding you give to the div.block element.
* If you want a pixel or em based space between the blocks you should apply this to an inner block, never to the block itself as this can cause the sum of block widths to be more than 100% of the available space.
*/
$vars['blockwidths'] = '';
if ($precount > 0) {
$prewidth = round(100/$precount,3);

$vars['blockwidths'] .= <<<EOD
.preblocks .block {
width: $prewidth%;
}
EOD;
}
if ($postcount > 0) {
$postwidth = round(100/$postcount,3);

$vars['blockwidths'] .= <<<EOD
.postblocks .block {
width:$postwidth%;
}
EOD;
}

// Sidebars And Middle column widths
$sidebarwidth = round(theme_get_setting('sidebar_width'),3);
if ((!isset($vars['page']['sidebar_first'])) && (!isset($vars['page']['sidebar_second']))) {
$mainwidth = 100;
} elseif ((isset($vars['page']['sidebar_first'])) && (isset($vars['page']['sidebar_second']))) {
$mainwidth = round((100-$sidebarwidth*2),3);
$sidebarwidth = $sidebarwidth - 0.001; // Where 0.001 is the sum of side-margin, side-border width and side-padding on the sidebars plus 0.001.
} elseif ((isset($vars['page']['sidebar_first'])) OR (isset($vars['page']['sidebar_second']))) {
$mainwidth = round((100-$sidebarwidth),3);
$sidebarwidth = $sidebarwidth - 0.001;
}
$vars['structure_widths'] = <<<EOD
.sidebar {width: {$sidebarwidth}%;}
#content {width: {$mainwidth}%;}
EOD;
//if the EOD; is the last line in this file a parse error occurs