<?php
  
/* Store theme paths in php and javascript variables */
GLOBAL $theme;
$theme_path = drupal_get_path('theme', $theme) .'/';
$abs_theme_path = base_path().$theme_path;
$files_path = file_stream_wrapper_get_instance_by_scheme('public')->getDirectoryPath();
/* Store theme paths in Drupal.settings JSON objects - Do not remove this! These bathe paths are used by various other scripts */
drupal_add_js(array('current_theme' => $theme), 'setting');
drupal_add_js(array('theme_path' => $abs_theme_path), 'setting');

if(is_file($theme_path . 'css/ie8fix.css')) {
  drupal_add_css($theme_path . 'css/ie8fix.css', array('weight' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 8', '!IE' => FALSE), 'preprocess' => FALSE));
}
if(is_file($theme_path . 'css/ie7fix.css')) {
  drupal_add_css($theme_path . 'css/ie7fix.css', array('weight' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'preprocess' => FALSE));
}
if(is_file($theme_path . 'css/ie6fix.css')) {
  drupal_add_css($theme_path . 'css/ie6fix.css', array('weight' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 6', '!IE' => FALSE), 'preprocess' => FALSE));
}

// include theme overrides
if(is_file($theme_path . 'theme-overrides.inc')) {
  include($theme_path . 'theme-overrides.inc');
}

// include theme functions
if(is_file($theme_path . 'theme-functions.inc')) {
  include($theme_path . 'theme-functions.inc');
}

// include theme settings controller
if(is_file($theme_path . 'theme-settings-controller.php')) {
  include($theme_path . 'theme-settings-controller.php');
}

/**
 * Implementation of hook_preprocess()
 *
 * This function checks to see if a hook has a preprocess file associated with
 * it, and if so, loads it.
 *
 * @param $vars
 * @param $hook
 * @return Array
 */
 /* if you rename the theme you have to change the the name of this function and of the drupal_get_path parameter */
function sooper_tundra_preprocess(&$vars, $hook) {
  if(is_file(drupal_get_path('theme', 'sooper_tundra') . '/preprocess/preprocess-' . str_replace('_', '-', $hook) . '.inc')) {
    include('preprocess/preprocess-' . str_replace('_', '-', $hook) . '.inc');
  }
}

/**
 * Override or insert variables into the page template for HTML output.
 */
function sooper_tundra_process_html(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_html_alter($variables);
  }
}
/**
 * Override or insert variables into the page template.
 */
function sooper_tundra_process_page(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_page_alter($variables);
  }
}

// Load the custom stylesheet.
if (is_file($theme_path . 'custom/style-custom.css')) {
  drupal_add_css($theme_path . 'custom/style-custom.css');
}
// Load custom template.php code
if (is_file($theme_path . 'custom/template-custom.php')) {
  include $theme_path . 'custom/template-custom.php';
}
