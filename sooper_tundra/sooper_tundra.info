name                                          = SOOPER Tundra
description                                   = A Starter Theme with Grid Layout System, jQuery Features, Custom Typography, Right-to-Left support, Color Module Integration and Skinr Integration. Sponsored by <a href="http://www.sooperthemes.com">SooperThemes</a>.
version                                       = 7.x-1.0
core                                          = 7.x
engine                                        = phptemplate

stylesheets[all][]                            = css/reset-preset.css
stylesheets[all][]                            = css/style.css

scripts[]                                     = scripts/misc/jquery.easing-sooper.js

regions[header]                               = header
regions[slideshow]                            = slideshow
regions[preblocks]                            = preblocks
regions[sidebar_first]                        = Sidebar first
regions[sidebar_second]                       = Sidebar second
regions[content_top]                          = content-top
regions[help] = Help
regions[page_top] = Page top
regions[page_bottom] = Page bottom
regions[content] = Content
regions[content_bottom] = content-bottom
regions[content]                              = content
regions[content_bottom]                       = content-bottom
regions[postblocks]                           = postblocks
regions[footer]                               = footer
regions[tundra-toolbar]                       = tundra-toolbar

features[]                                    = logo
features[]                                    = name
features[]                                    = slogan
features[]                                    = node_user_picture
features[]                                    = comment_user_picture
features[]                                    = search
features[]                                    = favicon
features[]                                    = main_menu
features[]                                    = secondary_menu

skinr[style][title]                         = Block Style
skinr[style][type]                          = select
skinr[style][description]                   = Present blocks in different styles
skinr[style][features][]                    = block
skinr[style][stylesheets][all][]            = css/skinr-skins.css
skinr[style][scripts][]                     = scripts/misc/jquery.corner.js
skinr[style][options][1][label]             = Rounded Corners
skinr[style][options][1][class]             = skinr-rounded

skinr[color][title]                         = Block Color
skinr[color][type]                          = select
skinr[color][description]                   = Present blocks in different colors
skinr[color][features][]                    = block
skinr[color][features][]                    = panels_pane
skinr[color][stylesheets][all][]            = css/skinr-skins.css
skinr[color][options][1][label]             = Primary Color
skinr[color][options][1][class]             = skinr-color
skinr[color][options][2][label]             = Secondary Color
skinr[color][options][2][class]             = skinr-color skinr-color-2
skinr[color][options][3][label]             = Text Color
skinr[color][options][3][class]             = skinr-color skinr-color-3
skinr[color][options][4][label]             = Primary Color - Block Title
skinr[color][options][4][class]             = skinr-color-title
skinr[color][options][5][label]             = Secondary Color - Block Title
skinr[color][options][5][class]             = skinr-color-title-2
skinr[color][options][6][label]             = Text Color - Block Title
skinr[color][options][6][class]             = skinr-color-title-3

skinr[block_size][title]                    = Block Width Override (Grid Control)
skinr[block_size][type]                     = select
skinr[block_size][description]              = Choose a block size
skinr[block_size][features][]               = block
skinr[block_size][features][]               = panels_pane
skinr[block_size][stylesheets][all][]       = css/skinr-skins.css
skinr[block_size][options][1][label]        = 75% (3/4)
skinr[block_size][options][1][class]        = grid-75
skinr[block_size][options][2][label]        = 66.7% (2/3)
skinr[block_size][options][2][class]        = grid-667
skinr[block_size][options][3][label]        = 50% (1/2)
skinr[block_size][options][3][class]        = grid-50
skinr[block_size][options][4][label]        = 33.3% (1/3)
skinr[block_size][options][4][class]        = grid-333
skinr[block_size][options][5][label]        = 25% (1/4)
skinr[block_size][options][5][class]        = grid-25
skinr[block_size][options][6][label]        = 20% (1/5)
skinr[block_size][options][6][class]        = grid-20
skinr[block_size][options][7][label]        = 16.7% (1/6)
skinr[block_size][options][7][class]        = grid-167
skinr[block_size][options][8][label]        = 12.5% (1/8)
skinr[block_size][options][8][class]        = grid-125
skinr[block_size][options][9][label]        = 11.1% (1/9)
skinr[block_size][options][9][class]        = grid-111
skinr[block_size][options][10][label]       = phi (0.618)
skinr[block_size][options][10][class]       = skinr-phi
skinr[block_size][options][11][label]       = phi2 (0.382)
skinr[block_size][options][11][class]       = skinr-phi2
skinr[block_size][options][12][label]       = phi3 (0.236)
skinr[block_size][options][12][class]       = skinr-phi3
skinr[block_size][options][13][label]       = Automatic Wrap
skinr[block_size][options][13][class]       = grid-auto

skinr[float][title]                         = Align Left/Right
skinr[float][type]                          = select
skinr[float][description]                   = Override left/right alignment (css float control)
skinr[float][features][]                    = block
skinr[float][options][1][label]             = Align Left
skinr[float][options][1][class]             = float-left
skinr[float][options][2][label]             = Align Right
skinr[float][options][2][class]             = float-right

skinr[font_size][title]                     = Font Size
skinr[font_size][type]                      = select
skinr[font_size][description]               = Choose a font size
skinr[font_size][features][]                = block
skinr[font_size][features][]                = panels_pane
skinr[font_size][stylesheets][all][]        = css/skinr-skins.css
skinr[font_size][options][1][label]         = 75%
skinr[font_size][options][1][class]         = skinr-xxs
skinr[font_size][options][2][label]         = 90%
skinr[font_size][options][2][class]         = skinr-xs
skinr[font_size][options][3][label]         = 125%
skinr[font_size][options][3][class]         = skinr-xl
skinr[font_size][options][4][label]         = 150%
skinr[font_size][options][4][class]         = skinr-xxl
skinr[font_size][options][5][label]         = 200%
skinr[font_size][options][5][class]         = skinr-xxxl

skinr[font_style][title]                    = Font Style
skinr[font_style][type]                     = checkboxes
skinr[font_style][description]              = Choose a font style
skinr[font_style][features][]               = block
skinr[font_style][features][]               = panels_pane
skinr[font_style][stylesheets][all][]       = css/skinr-skins.css
skinr[font_style][options][1][label]        = Bold
skinr[font_style][options][1][class]        = skinr-bold
skinr[font_style][options][2][label]        = Italic
skinr[font_style][options][2][class]        = skinr-italic
skinr[font_style][options][3][label]        = Underlined
skinr[font_style][options][3][class]        = skinr-underlined
skinr[font_style][options][4][label]        = Cufon
skinr[font_style][options][4][class]        = cufon

;Theme Settings Defaults

;Other Options
settings[iepngfix]                            = 1
settings[force_eq_heights]                    = 1

;Sooper LayoutKit
settings[fixedfluid]                          = px
settings[layout_width_fluid]                  = 80
settings[layout_width_fixed]                  = 960
settings[sidebar_width]                       = 25
settings[expand_admin]                        = 0
settings[layout_min_width]                    = 0
settings[layout_max_width]                    = 0

;Sooper DropdownKit
settings[sooperfish_width]                    = 200
settings[sooperfish_enable]                   = 1
settings[sooperfish_invoke]                   = #navbar > ul.menu
settings[sooperfish_delay]                    = 500
settings[sooperfish_dualcolumn]               = 5
settings[sooperfish_triplecolumn]             = 12
settings[sooperfish_speed_show]               = 500
settings[sooperfish_speed_hide]               = 500
settings[sooperfish_easing_show]              = swing
settings[sooperfish_easing_hide]              = swing
settings[sooperfish_properties_show][opacity] = 0
settings[sooperfish_properties_show][height]  = 1
settings[sooperfish_properties_show][width]   = 0
settings[sooperfish_properties_hide][opacity] = 1
settings[sooperfish_properties_hide][height]  = 1
settings[sooperfish_properties_hide][width]   = 0

;Sooper FontKit
settings[headings_font_face]                  = helvetica
settings[body_font_face]                      = helvetica
settings[cufon_enable]                        = 1
settings[cufon_invoke]                        = h1, h2, h3, h4, h5, h6, dt, p.mission, p.slogan, legend:not(.collapse-processed)
settings[cufon_font_face]                     = Sapir.js

;Sooper SlideshowKit
settings[slideshowkit_enable]                   = 1
settings[cycle_invoke]                        = div.slideshow div>ul
settings[cycle_fx]                            = scrollHorz
settings[cycle_timeout]                       = 4000
settings[cycle_continuous]                    = 0
settings[cycle_speed]                         = 800
settings[cycle_pagerEvent]                    = mouseover
settings[cycle_easing]                        = linear
settings[cycle_random]                        = 0
settings[cycle_pause]                         = 0
settings[cycle_pauseOnPagerHover]             = 1
settings[cycle_delay]                         = 0

;Sooper Linkicons
settings[link_icons_enable]                   = 1
settings[link_icons_set]                      = fugue
