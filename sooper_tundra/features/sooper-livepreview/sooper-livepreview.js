$ = jQuery.noConflict().noConflict(); // Make sure jQuery owns $ here
 

$(document).ready(function(){

  var initPreview;
  var initToucan;
  var themePath = Drupal.settings.theme_path;
  var currentTheme = Drupal.settings.current_theme;
  var themeSettingsPage = Drupal.settings.t_s_page;
  var selectedBackground = '';

  // Update colors when farbtastic is updated
  $('.color-form input').click(function(event){
   updateColors();
  });

  $('.color-form input').keyup(function(event){
   updateColors();
  });

  $('.color-form select').click(function(event){
   updateColors();
  });

  $('.color-form select').keyup(function(event){
   updateColors();
  });

  $(document).bind('mousemove', checkDrag).bind('mouseup', checkDrag);
 
  function checkDrag() {
    if (document.dragging) {
      updateColors();
    }
  }

  function updateColors() {

    if (!initPreview) {
      initPreview = true;
      // Set warning message
      // Set warning message
      $('#content .inner').prepend('<div class="messages warning sooper-livepreview-mssg">You are tuning colors in preview mode. Preview mode has some imperfections and will only give an approximate preview of the colorscheme. <strong>Submit the form to view the configuration in full effect</strong>.</div>');

      // Replace normal images with PNG ones
      $('#header img.logo').attr('src',''+themePath+'images/transparent/logo.png');
      $('#header form#search-theme-form input.form-submit, #navbar ul li.backLava div').css('background-image','url('+themePath+'images/transparent/sprite-color.png)');
      
      toucan();
    }
    // From selectors as in the CSS theme-options file rendering in template.php... deleted all the selectors that are not needed on the colorform page to improve farbtastic performance.
    $('.content a, #header h1#sitename a, div.slideshow h2, .slideshow ul li .views-field-title, legend').css('color',$('#edit-palette-base').val());
    $('body, #navbar ul li a, ul.primary li a, ul.secondary a').css('color',$('#edit-palette-text').val()); // dit overwrite alle css van alle tekst :|
    $('.poll .bar .foreground, input.form-submit, .block.callout-1, .block.callout-1 .blocktitle span, #navbar ul ul li.backLava, #navbar ul li.backLava div, div.slideshow ul li .readmore, div.slideshow ul li .read-more, div.slideshow ul li .read_more, div.slideshow ul li .views-field-view-node').css('background-color',$('#edit-palette-base').val());
    $('.block.callout-1').css('border-color',$('#edit-palette-base').val());
    $('#header .form-submit').css('color',$('#edit-palette-link').val());
    $('div.slideshow ul li div, div.slideshow ul li p, div.slideshow ul li span, .block.callout-2, .block.callout-2 .blocktitle span').css('background-color',$('#edit-palette-link').val());
    $('.block.callout-2').css('border-color',$('#edit-palette-link').val());
    $('body').css('background-color',$('#edit-palette-top').val());
    if(window.Cufon) {
    Cufon.refresh();
    }
  }

  // Updating Background Image when selection changes
  $('#edit-background-image').click(function(event){
   updateBackground();
  });

  $('#edit-background-image').keyup(function(event){
   updateBackground();
  });

  function updateBackground() {
    selectedBackground = $('#edit-background-image').val()
    $('body').css('background-image','url('+themePath+'images/backgrounds/'+selectedBackground+')');
    
    toucan();
  }

  // Update layout when LivePreview button is clicked
  var containerWidth = '';
  var selectedUnit = '';
  var sidebarWidth = '';
  var singleWidth = '';
  var dualWidth = '';

  $('#live-preview-layout').click(function(event){
   updateLayout();
  });

  function updateLayout() {
  $('#container').css('min-width',$('#edit-layout-min-width').val());

  if ($('#edit-layout-max-width').val() > 0) {
    $('#container').css('max-width',$('#edit-layout-max-width').val());
  } else {
    $('#container').css('max-width','none');
  }

    selectedUnit = $("input[@name='fixedfluid']:checked").val();
    widthFluid = $('#edit-layout-width-fluid').val();
    widthFixed = $('#edit-layout-width-fixed').val();
    if (selectedUnit == 'px') {
      $('#container').animate({'width': widthFixed+'px'}, '1000ms');
    } else {
      $('#container').animate({'width': widthFluid+'%'}, '1000ms');
    }

    sidebarWidth = Math.round($('#edit-sidebar-width').val()*1000)/1000;
    dualWidth = Math.round((100-$('#edit-sidebar-width').val()*2)*1000)/1000;
    singleWidth = Math.round((100-$('#edit-sidebar-width').val())*1000)/1000;
    $('body.two-sidebars #content').animate({'width': dualWidth+'%'}, '1000ms');
    $('body.one-sidebar #content').animate({'width': singleWidth+'%'}, '1000ms');
    $('.sidebar').animate({'width': sidebarWidth+'%'}, '1000ms');
    
    toucan();
  }

  // Update Typography when selection changes 
  $('#edit-headings-font-face, #edit-body-font-face').click(function(event){
   updateType();
  });

  $('#edit-headings-font-face, #edit-body-font-face').keyup(function(event){
   updateType();
  });

  function updateType() {
    bodyType = $('#edit-body-font-face').val();
    headingType = $('#edit-headings-font-face').val();
        
    $('body').css('font-family',fontFamily(bodyType));
    $('h1, h2, h3, h4, h5, h6, legend, p.mission, p.slogan').css('font-family',fontFamily(headingType));
    
    // Javascript does not support associative arrays :(
    function fontFamily(fontKey) {
      if (fontKey == 'helvetica') { family = 'Arial, Helvetica, "Helvetica Neue", "Nimbus Sans L", "Liberation Sans", "FreeSans", sans-serif'; }
      if (fontKey == 'verdana') { family = 'Verdana, "Bitstream Vera Sans", Arial, sans-serif'; }
      if (fontKey == 'lucida') { family = '"Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", "DejaVu Sans", Arial, sans-serif'; }
      if (fontKey == 'geneva') { family = '"Geneva", "Bitstream Vera Serif", "Tahoma", sans-serif'; }
      if (fontKey == 'tahoma') { family = 'Tahoma, Geneva, "DejaVu Sans Condensed", sans-serif'; }
      if (fontKey == 'century') { family = '"Century Gothic", "URW Gothic L", Helvetica, Arial, sans-serif'; }
      if (fontKey == 'georgia') { family = 'Georgia, "Bitstream Vera Serif", serif'; }
      if (fontKey == 'palatino') { family = '"Palatino Linotype", "URW Palladio L", "Book Antiqua", "Palatino", serif'; }
      if (fontKey == 'times') { family = '"Times New Roman", Cambria, "Free Serif", Times, serif'; }
      
      return family;
    }
    
    toucan();
  }
  
  function toucan() {
    if (!initToucan) {
      initToucan = true;
      $('body').append('<div class="cornerstone toucan"></div>');
      $('.toucan').animate({'bottom': '-10px'},'2000ms');
    }
  }


});