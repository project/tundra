<?php
$sooperdefaults = array(
  'fixedfluid'                        => 'px',
  'layout_width_fluid'                => 80,
  'layout_width_fixed'                => 960,
  'sidebar_width'                     => 25,
  'expand_admin'                      => 0,
  'layout_min_width'                  => 0,
  'layout_max_width'                  => 0,
);