<?php

  $defaults = array(
	// Other Options
    'iepngfix'                          => 1,
    'force_eq_heights'                  => 1,
  );

  // Merge with Defaults from the features
  
  // Load SOOPER Features
  foreach (file_scan_directory($theme_path .'features', '/defaults.inc/i') as $file) {
    include($file->uri);
    $defaults = array_merge($defaults, $sooperdefaults);    
  }